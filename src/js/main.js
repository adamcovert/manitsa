$(document).ready(function () {

  var scrollEl = document.querySelector('.rellax')

  if (scrollEl) {
    var rellax = new Rellax('.rellax');
  }

  $('.burger').on('click', function () {
    $('.mobile-menu').addClass('mobile-menu--is-open');
  });

  $('.mobile-menu__close').on('click', function () {
    $('.mobile-menu').removeClass('mobile-menu--is-open');
  });

  var swiper = new Swiper ('.promo__slider', {
    loop: true,
    autoplay: {
      delay: 7500,
    },
    speed: 750,
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    pagination: {
      el: '.promo__slider-pagination',
      clickable: true
    },
    allowTouchMove: true
  });

  var swiper = new Swiper ('.popular__slider', {
    spaceBetween: 40,
    pagination: {
      el: '.popular__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.popular__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.popular__slider-navigation .slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      414: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 40,
      }
    }
  });

  var galleryThumbs = new Swiper('.product__gallery-thumbs', {
    slidesPerView: 3,
    watchOverflow: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    direction: 'vertical',
    slidesPerView: 'auto',
    spaceBetween: 15,
  });

  var galleryMain = new Swiper('.product__gallery-main', {
    mousewheel: true,
    watchOverflow: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    preventInteractionOnTransition: true,
    pagination: {
      el: '.product__gallery-progressbar',
      type: 'progressbar'
    },
    breakpoints: {
      320: {
        direction: 'horizontal'
      },
      992: {
        direction: 'vertical',
      }
    },
    thumbs: {
      swiper: galleryThumbs
    }
  });

  galleryMain.on('slideChangeTransitionStart', function () {
    galleryThumbs.slideTo(galleryMain.activeIndex);
  });

  galleryThumbs.on('transitionStart', function () {
    galleryMain.slideTo(galleryThumbs.activeIndex);
  });



  $('.order-card__minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });

  $('.order-card__plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
  });
});